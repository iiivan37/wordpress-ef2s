# Wordpress

L'interface d'administration

---

### Connexion

Commencez par vous connecter à l'espace d'administration de votre site web. Accédez à la page de connexion de votre site en ajoutant "wp-admin" après votre nom de domaine (ex. ```http://example.com/wp-admin```).

---

![login](img/login_form.png)

---

### Premiers pas

Une fois connecté vous vous trouvez sur le Panneau d'Administration appelé Tableau de bord.

---

![Tableau de bord](img/800px-dashboard.png)
---

L’interface d’administration vous permet d’accéder aux fonctionnalités de gestion de votre site WordPress.
Chaque panneau d’administration est divisé en sections :
---

![Interface d'administration](img/interface-admin.png)

---

 ### Pages / articles / catégories / commentaires

---

Wordpress fournit des outils permettant de mêler les notions de site (organisation thématique des contenus) et de blog (organisation chronologique des contenus).


---

### Pages
Une page est un ensemble de contenus (textes, images, sons...) fournissant des informations plutôt pérennes : présentation de l'école, des classes, contacts... ; c'est l'aspect "site" de Wordpress. Les titres des pages s'affichent automatiquement dans le bandeau supérieur du thème (si elles deviennent nombreuses, on peut les organiser en menus déroulants)

---

### Articles
Un article contient des éléments plus dynamiques, liés à la vie quotidienne du site, actualités et comptes-rendus d'activités au jour le jour ; c'est l'aspect "blog" de Wordpress.

---

### Catégories
Les catégories permettent d'indexer les articles, c'est à dire de les classer dans des thèmes pré-définis ; cet outil permet d'organiser les articles selon des critères autres que chronologique.

---

### Commentaires
Pour chaque page ou article publié, les visiteurs du site peuvent laisser des commentaires qui viennent s'afficher sous le contenu ; les commentaires permettent une interaction et une participation plus active des visiteurs.

---

### Ecrire un article ou une page
La publication de contenus se passe de façon identique, dans une page ou dans un article.

---

Un éditeur permet de saisir et de mettre en forme du texte, dans des conditions assez proches d'un traitement de texte classique (saisie, barres d'outils...)
![Editeur](img/editeur.gif)
---
On peut transformer un mot, un groupe de mots ou une image en lien vers un autre article/page du site ou vers un site web quelconque.
![Liens](img/liens.gif)

---

### Médias (images/galeries, sons, vidéos)

Dans les articles comme dans les pages, on peut ajouter des photos (individuellement entre des paragraphes de texte ou regroupées dans une galerie), des sons et des vidéos.

---

![medias](img/bib_medias.gif)
---


### Utilisateurs / statuts

Différents statuts, octroyant différents droits, peuvent être attribués aux utilisateurs d'un site, depuis le simple visiteur qui ne peut que lire les pages et articles jusqu'à l'administrateur qui peut tout modifier dans le site.

[Rôles et Permissions](https://codex.wordpress.org/R%C3%B4les_et_Permissions)

---

### Apparence : thèmes / widgets

---
#### Thèmes
Les thèmes sont les outils de base pour modifier l'apparence d'un site Wordpress : changer de thème, c'est modifier la présentation du site, tout en conservant les contenus.

---

![themes](img/themes.gif)

---
#### Widgets
Au sein de chaque thème, vous pouvez également choisir les modules à afficher dans les barres latérales (sidebars) de votre site.

Ces blocs, appelés widgets, contiennent des éléments d'information de différentes natures : fonction de recherche, calendrier, liste des catégories, archives...

---
![Widgets](img/widgets.gif)
---
